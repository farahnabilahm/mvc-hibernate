/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import javax.swing.JOptionPane;

/**
 *
 * @author ZANDUT
 */
public interface InterfaceView
{
    public int showConfirmation(String message);
    
    public void showMessage(String message);
    
    public void showMessage(String message, String title, int type);
    
}
