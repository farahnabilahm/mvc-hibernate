/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Application;
import Models.Mahasiswa;
import Views.MainFrame;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author PRAKTIKUM
 */
public class ControllerMainFrame extends MouseAdapter implements ActionListener, KeyListener{

    private Application apps = new Application();
    private MainFrame main = new  MainFrame();

    private String[] columnTableMahasiswa = {"NIM", "NAMA"};
    
    public void initTheme()
    {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
    public ControllerMainFrame() {
        initTheme();
        main = new MainFrame();
        
        
        main.getjButton1().addActionListener(this);
        main.getjButton2().addActionListener(this);
        main.getjButton3().addActionListener(this);
        
        main.getjTable1().addKeyListener(this);
        
        main.setResizable(false);
        main.setLocationRelativeTo(null);
        main.setVisible(true);
    }
    
    public void show(JPanel panel, String cardName)
    {
        CardLayout card = (CardLayout) panel.getLayout();
        card.show(panel, cardName);
    }
    
    public void initJtable(String[] column, JTable table)
    {
        DefaultTableModel tb = new DefaultTableModel(column, 0){

            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
           
        };
        
        table.setModel(tb);
    }
    
    public void addItemToTable(JTable table, List<Mahasiswa> data)
    {
        DefaultTableModel tb = (DefaultTableModel) table.getModel();
        for (Mahasiswa m : data)
        {
            String[] row = {m.getNim(), m.getNama()};
            tb.addRow(row);
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        Object a = e.getSource();
        if (a.equals(main.getjButton1()))
        {
            try {
                if (apps.insertMahasiswa(new Mahasiswa(main.getjTextField2().getText(), main.getjTextField1().getText())))
                {
                    main.showMessage("Insert Berhasil !!!");
                }
               
            } catch (Exception ex) {
                ex.printStackTrace();
                main.showMessage("Insert Gagal !!\n"+ex.getLocalizedMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
            }
            
        }
        
        if (a.equals(main.getjButton2()))
        {
            initJtable(columnTableMahasiswa, main.getjTable1());
            
            addItemToTable(main.getjTable1(), apps.getAll());
            show(main.getPanel_dasar(), "view");
            
        }
        
        if (a.equals(main.getjButton3()))
        {
            main.getjTextField1().setText("");
            main.getjTextField2().setText("");
            show(main.getPanel_dasar(), "insert");
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        
    }

    @Override
    public void keyTyped(KeyEvent e)
    {
        
    }

    @Override
    public void keyPressed(KeyEvent e)
    {
        Object a = e.getSource();
        if (a.equals(main.getjTable1()))
        {
            if (e.getKeyCode() == e.VK_ENTER)
            {
                main.showMessage("NAMA : "+main.getjTable1().getValueAt(main.getjTable1().getSelectedRow(), 1).toString());
            }
            
            if (e.getKeyCode() == e.VK_DELETE)
            {
                String nim = main.getjTable1().getValueAt(main.getjTable1().getSelectedRow(), 0).toString();
                Mahasiswa m = apps.getMahasiswaByNim(nim);
                
                if (JOptionPane.YES_OPTION == main.showConfirmation("Yakin menghapus ? "))
                {
                    try
                    {
                        if (apps.deleteMahasiswa(m))
                        {
                            main.showMessage("Delete berhasil !!");
                            
                            initJtable(columnTableMahasiswa, main.getjTable1());
                            addItemToTable(main.getjTable1(), apps.getAll());
                        }
                    } catch (Exception ex)
                    {
                        main.showMessage("Delete Gagal !!\n"+ex.getLocalizedMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
                    }
                    
                }
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e)
    {
        
    }
    
    
    
}
